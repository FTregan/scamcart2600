/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
static const uint8_t rom[4096];
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define led_Pin GPIO_PIN_13
#define led_GPIO_Port GPIOC
#define key_Pin GPIO_PIN_0
#define key_GPIO_Port GPIOA
#define data1_Pin GPIO_PIN_1
#define data1_GPIO_Port GPIOA
#define data2_Pin GPIO_PIN_2
#define data2_GPIO_Port GPIOA
#define data3_Pin GPIO_PIN_3
#define data3_GPIO_Port GPIOA
#define data4_Pin GPIO_PIN_4
#define data4_GPIO_Port GPIOA
#define data5_Pin GPIO_PIN_5
#define data5_GPIO_Port GPIOA
#define addr0_Pin GPIO_PIN_0
#define addr0_GPIO_Port GPIOB
#define addr1_Pin GPIO_PIN_1
#define addr1_GPIO_Port GPIOB
#define boot1_Pin GPIO_PIN_2
#define boot1_GPIO_Port GPIOB
#define addr9_Pin GPIO_PIN_10
#define addr9_GPIO_Port GPIOB
#define addr10_Pin GPIO_PIN_12
#define addr10_GPIO_Port GPIOB
#define addr11_Pin GPIO_PIN_13
#define addr11_GPIO_Port GPIOB
#define addr12_Pin GPIO_PIN_14
#define addr12_GPIO_Port GPIOB
#define data6_Pin GPIO_PIN_8
#define data6_GPIO_Port GPIOA
#define data7_Pin GPIO_PIN_9
#define data7_GPIO_Port GPIOA
#define data8_Pin GPIO_PIN_10
#define data8_GPIO_Port GPIOA
#define addr2_Pin GPIO_PIN_3
#define addr2_GPIO_Port GPIOB
#define addr3_Pin GPIO_PIN_4
#define addr3_GPIO_Port GPIOB
#define addr4_Pin GPIO_PIN_5
#define addr4_GPIO_Port GPIOB
#define addr5_Pin GPIO_PIN_6
#define addr5_GPIO_Port GPIOB
#define addr6_Pin GPIO_PIN_7
#define addr6_GPIO_Port GPIOB
#define addr7_Pin GPIO_PIN_8
#define addr7_GPIO_Port GPIOB
#define addr8_Pin GPIO_PIN_9
#define addr8_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
